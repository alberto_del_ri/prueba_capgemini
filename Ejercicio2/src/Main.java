
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {

	public static void main(String[] args) {

		List<Integer> datosOfrecidos = new ArrayList<Integer>();
		datosOfrecidos.add(3);
		datosOfrecidos.add(1);
		datosOfrecidos.add(6);
		datosOfrecidos.add(4);
		datosOfrecidos.add(10);

		calculMaximoMinimo(datosOfrecidos);

	}

	static void calculMaximoMinimo(List<Integer> datosOfrecidos) {

		Collections.sort(datosOfrecidos);
	
		
		List<Integer> maximo = datosOfrecidos.subList(1, 5);
		List<Integer> minimo = datosOfrecidos.subList(0, 4);


		 
		System.out.println("El numero minimo es :" + minimo + "="+ minimo.stream().reduce(0, (x,y) -> x+y)+
				", el numero maximo es :" + maximo + "="+ maximo.stream().reduce(0, (x,y) -> x+y));

	}
}
