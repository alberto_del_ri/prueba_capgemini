
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		calculRestaDiagonales();

	}

	static void calculRestaDiagonales() {

		int[][] matriz = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		int h = matriz.length - 1;
		int valorPrimero = 0;
		int valorSegundo = 0;

		for (int i = 0; i <= matriz.length - 1; i++) {
			
			valorPrimero = valorPrimero + matriz[i][i];
			valorSegundo = valorSegundo + matriz[h][i];
			h--;
		}

		System.out.println("Valores 1 diagonal - valores 2 diagonal " + (valorPrimero - valorSegundo));
	}

}
